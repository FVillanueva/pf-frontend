MVC.Controller = class Controller {
    constructor(props) {
            this.eventHandler()
            this.model = new props.model(props.endpoint)
            this.view = new props.view(props.contentElement)
            this.eventAddPost()
                // this.eventDelete()
        }
        // eventDelete() {
        //     document.body.addEventListener('click', (e) => {
        //         this.dPost();
        //     })
        // }

    eventAddPost() {
        var formulario = document.getElementById('formulario');

        formulario.addEventListener('submit', (e) => {
            // e.preventDefault();
            this.postData()
        })
    }
    eventHandler() {
        document.body.addEventListener("onloadApp", (event) => {
            this.getData()
        })
    }

    getData() {
        this.model.getArticulo()
            .then(data => {
                this.view.notify(data)
            })
            .catch(console.log)
    }

    postData() {
        let variable = this.view.addPost()
        console.log(variable);

        this.model.postArticulo(variable);
    }

    // dPost() {
    //     let post = this.view.deleteP()
    //         // console.log(post);
    //     this.model.deletePost(post);

    // }

}