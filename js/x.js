'use strict';
//Consultar API con metodo fetch (Metodo sustituto de ajax  que hace peticiones ajax∫)
var post = [];
var section_usuarios = document.querySelector('#articleP');

fetch('http://localhost:8080/api/post')
    //promesa con metodo then para recoger datos
    //recibe el parametro data y lo convierte a json con metodo .json
    .then(data => data.json())
    //recogemos la data de nuevo y la guardamos en un array
    .then(article => {
        post = article;
        console.log(post);
        getUsers();
    });

function getUsers() {
    setTimeout(() => {
        //map()
        post.map((value, index) => {
            let title,
                article,
                author,
                category;

            title = document.createElement('h4');
            title.setAttribute('class', 'blue-text');

            article = document.createElement('h5');
            article.setAttribute('class', '  blue-grey-text');

            author = document.createElement('p');
            author.setAttribute('class', 'pink-text');

            category = document.createElement('p');
            category.setAttribute('class', 'blue-grey lighten-4')

            title.innerText = `${ value.titulo }`;
            article.innerHTML = `${ value.articulo}`;
            author.innerHTML = `${ value.autor}`;
            category.innerHTML = `${ value.categoria}`;

            section_usuarios.appendChild(title);
            section_usuarios.appendChild(article);
            section_usuarios.appendChild(author);
            section_usuarios.appendChild(category);

            document.querySelector('.loading').style.display = 'none';
        });

    }, 3000);
}