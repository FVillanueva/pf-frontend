MVC.View = class View {
    constructor(elem) {
        this.eventHandler()
        this.elem = elem
    }
    eventHandler() {
        document.body.addEventListener("onLoadData", (event) => {
            this.updateView(event.detail)
        })
    }
    notify(data) {
        const onLoadDataEvent = new CustomEvent("onLoadData", { detail: data, bubbles: true })
        console.log(data);
        this.elem.dispatchEvent(onLoadDataEvent)
    }
    updateView(data) {

        var post = [];
        var section_usuarios = document.querySelector('#articleP');
        post = data;
        setTimeout(() => {
            //map()
            post.map((value, index) => {
                let title,
                    article,
                    author,
                    category,
                    edit,
                    del,
                    space;

                //Create elements
                title = document.createElement('h4');
                title.setAttribute('class', 'blue-text');

                article = document.createElement('h5');
                article.setAttribute('class', '  blue-grey-text');

                author = document.createElement('p');
                author.setAttribute('class', 'pink-text');

                category = document.createElement('p');
                category.setAttribute('class', 'grey lighten-3')

                edit = document.createElement('button');
                edit.setAttribute('data-id', value.id);
                edit.insertAdjacentText('beforeend', 'Edit')
                edit.setAttribute('id', value.id);
                edit.setAttribute('type', 'submit');
                edit.setAttribute('class', 'btn waves-effect waves-light blue-grey darken-3')

                del = document.createElement('button');
                del.setAttribute('data-id', value.id);
                del.insertAdjacentText('beforeend', 'Delete');
                del.setAttribute('id', value.id);
                del.setAttribute('type', 'submit');
                del.setAttribute('class', 'btn waves-effect waves-light pink darken-1');

                space = document.createElement('hr');
                space.setAttribute('class', 'grey lighten-5')

                title.innerText = `${ value.titulo }`;
                article.innerText = `${ value.articulo}`;
                author.innerText = `${ value.autor}`;
                category.innerText = `${ value.categoria}`;

                section_usuarios.appendChild(title);
                section_usuarios.appendChild(article);
                section_usuarios.appendChild(author);
                section_usuarios.appendChild(category);
                section_usuarios.appendChild(edit);
                section_usuarios.appendChild(del);
                section_usuarios.appendChild(space)

                document.querySelector('.loading').style.display = 'none';


            });
            return post;

            console.log('cargado');
        }, 3000);
    }

    addPost() {
        var data = new FormData(formulario);

        let dataF = {
                'titulo': data.get('titulo'),
                'articulo': data.get('articulo'),
                'autor': data.get('autor'),
                'categoria': data.get('categoria')

            }
            // console.log(dataF);
        return dataF;
    }

    // deleteP(post) {
    //     let btn_id = [];
    //     let seleccion_btn = document.querySelector('#numerosD');
    //     seleccion_btn.getAttribute(['data-id']);

    //     console.log(seleccion_btn);


    // }
}