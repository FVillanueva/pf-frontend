function countChars(obj) {
    var maxLength = 150;
    var strLength = obj.value.length;

    if (strLength > maxLength) {
        document.getElementById("charNum").innerHTML = '<span style="color: red;">' + strLength + ' out of ' + maxLength + ' characters</span>';
    } else {
        document.getElementById("charNum").innerHTML = strLength + '/' + maxLength;
    }
}

function countCharsTitle(obj) {
    var maxLength = 20;
    var strLength = obj.value.length;

    if (strLength > maxLength) {
        document.getElementById("charNumT").innerHTML = '<span style="color: red;">' + strLength + ' out of ' + maxLength + ' characters</span>';
    } else {
        document.getElementById("charNumT").innerHTML = strLength + '/' + maxLength;
    }
}

function countCharsAuthor(obj) {
    var maxLength = 25;
    var strLength = obj.value.length;

    if (strLength > maxLength) {
        document.getElementById("charNumA").innerHTML = '<span style="color: red;">' + strLength + ' out of ' + maxLength + ' characters</span>';
    } else {
        document.getElementById("charNumA").innerHTML = strLength + '/' + maxLength;
    }
}

function countCharsCategory(obj) {
    var maxLength = 15;
    var strLength = obj.value.length;

    if (strLength > maxLength) {
        document.getElementById("charNumC").innerHTML = '<span style="color: red;">' + strLength + ' out of ' + maxLength + ' characters</span>';
    } else {
        document.getElementById("charNumC").innerHTML = strLength + '/' + maxLength;
    }
}