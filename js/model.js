MVC.Model = class Model {
    constructor(endpoint) {
        this.endpoint = endpoint
        this.modelData = {}
    }

    getArticulo() {
        return fetch(`${this.endpoint}`)
            .then(resp => {
                if (resp.ok) {
                    return resp.json()
                }
                return Error("No se pudieron obtener los datos")
            })
            .then(data => {
                this.setModelo(data)
                return data
            })
    }
    setModelo(data) {
        this.modelData = data
    }

    postArticulo(dataF) {
        return fetch(`${this.endpoint}`, {
                credentials: 'same-origin', // 'include', default: 'omit'
                method: 'POST', // 'GET', 'PUT', 'DELETE', etc.
                body: JSON.stringify(dataF), // Coordinate the body type with 'Content-Type'
                headers: new Headers({
                    'Content-Type': 'application/json'
                }),
            })
            // .then(response => response.json())
            .then(dataF => console.log(dataF)) // Result from the `response.json()` call
            .catch(error => console.error(error))
    }

    // deletePost(data) {
    //     return fetch(`${this.endpoint}`, {
    //             method: "DELETE",
    //             body: JSON.stringify(data),
    //             headers: new Headers({
    //                 'Content-Type': 'application/json'
    //             }),
    //         })
    //         .then(data => console.log(data))
    //         .catch(error => console.log(error))
    // }
}